## README:

Ruby script to generate a variety of random outcomes for Magic: The Gathering

## TO USE:

Obviously you need Ruby. Any recent version should do

You'll also need the restclient and json gems, if not already installed

Run the script: (*ruby mtgRandomizer.rb*) or (**chmod +x** the file and *./mtgRandomizer.rb*)

Follow the prompts and choice your random selections

## NOTE:

Output of this script is formatted to be pastable into Discord. To accurately use this functionally, edit the *mtgRandomizer.rb* file and enter your player names on line 6.

Sets, Blocks and Planes are up to date as of `2023-03-09`. To add additional entries, append to the entries on lines `212`, `214` and `216`.
