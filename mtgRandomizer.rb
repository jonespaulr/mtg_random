#!/usr/bin/ruby

require 'rest-client'
require 'json'

$players = ["@Player1", "@Player2", "@Player3", "@jonespaulr"] 

def colourPicker
  ravColours = ["Selesnya :manaw::manag:", "Orzhov :manaw::manab:", "Boros :manaw::manar:", "Azorius :manaw:manau:", "Dimir :manau::manab:", "Rakdos :manab::manar:", "Izzet :manau::manar:", "Simic :manau::manag:", "Gruul :manar::manag:"]

  allColours = [
  "White :manaw:", "Blue :manau:", "Black :manab:", "Red :manar:", "Green :manag:", "Selesnya :manaw::manag:", "Orzhov :manaw::manab:", "Boros :manaw::manar:", "Azorius :manaw:manau:", "Dimir :manau::manab:", "Rakdos :manab::manar:", "Izzet :manau::manar:", "Simic :manau::manag:", "Gruul :manar::manag:", "Naya :manaw::manar::manag:", "Esper :manaw::manau::manab:", "Grixis :manau::manab::manar:", "Jund :manab::manar::manag:", "Bant :manaw::manau::manag:", "Abzan :manaw::manab::manag:", "Temur :manau::manar::manag:", "Jeskai :manaw::manau::manar:", "Mardu :manaw::manab::manar:", "Sultai :manau::manab::manag:", "Glint :manau::manab::manar::manag:", "Dune :manaw::manab::manar::manag:", "Ink :manaw::manau::manar::manag:", "Witch :manaw::manau::manab::manag:", "Yore :manaw::manau::manab::manar:", "Five Colour :manaw::manau::manab::manar::manag:", "Colourless"]

  threeColour = [
  "Naya :manaw::manar::manag:", "Esper :manaw::manau::manab:", "Grixis :manau::manab::manar:", "Jund :manab::manar::manag:", "Bant :manaw::manau::manag:", "Abzan :manaw::manab::manag:", "Temur :manau::manar::manag:", "Jeskai :manaw::manau::manar:", "Mardu :manaw::manab::manar:", "Sultai :manau::manab::manag:"]

  singleColour = [
  "White :manaw:", "Blue :manau:", "Black :manab:", "Red :manar:", "Green :manag:"]

  puts "Please make a selection:"
  puts
  puts "(1) Ravnica Guild Colours"
  puts "(2) All Colours"
  puts "(3) Single Colour"
  puts "(4) Shard and Wedge Colours"

  choice = gets.chomp!

  if choice == "1" 
    $players.each {|i| puts "#{i} builds #{ravColours.sample}"}
  elsif choice == "2" 
    $players.each {|i| puts "#{i} builds #{allColours.sample}"}
  elsif choice == "3" 
    $players.each {|i| puts "#{i} builds #{singleColour.sample}"}
  else
    $players.each {|i| puts "#{i} builds #{threeColour.sample}"}
  end
end

def background
  url = 'https://api.scryfall.com/cards/search?q=is%3Acommander+o%3Abackground'

  pages = []

  loop do
    response = RestClient.get(url)
    more = JSON.parse(response)['has_more'].to_s
    if more == "true"
      pages.push url
      url = JSON.parse(response)['next_page']
    else
      pages.push url
      break 
    end
  end

  url2 = 'https://api.scryfall.com/cards/search?q=type%3Abackground'

  pages2 = []

  loop do
    response = RestClient.get(url2)
    more = JSON.parse(response)['has_more'].to_s
    if more == "true"
      pages2.push url2
      url = JSON.parse(response)['next_page']
    else
      pages2.push url2
      break 
    end
  end

  background_commanders = []

  pages.each do |x|
    response = RestClient.get(x)
    result = JSON.parse(response)['data']
    result.each do |i|
       background_commanders.push i['name']
    end
  end

  background_commanders.map! { |x| x.to_s.gsub('"', '') }

  backgrounds = []

  pages2.each do |x|
    response = RestClient.get(x)
    result = JSON.parse(response)['data']
    result.each do |i|
      backgrounds.push i['name']
    end
  end

  backgrounds.map! { |x| x.to_s.gsub('"', '') }

 $players.each {|i| puts "#{i} builds [[#{background_commanders.sample}]] with [[#{backgrounds.sample}]]"}

end

def jankCommander
  url = 'https://api.scryfall.com/cards/search?q=usd%3C%3D0.79+is%3Acommander&unique=cards'

  pages = []

  loop do
    response = RestClient.get(url)
    more = JSON.parse(response)['has_more'].to_s
    if more == "true"
      pages.push url
      url = JSON.parse(response)['next_page']
    else
      pages.push url
      break 
    end
  end

  jank_commanders = []

  pages.each do |x|
    response = RestClient.get(x)
    result = JSON.parse(response)['data']
    result.each do |i|
      jank_commanders.push i['name']
    end
  end

  $players.each {|i| puts "#{i} builds [[#{jank_commanders.sample}]]"}
end

def oldCommander
  url = 'https://api.scryfall.com/cards/search?q=date<%3Dnph+is%3Acommander'

  pages = []

  loop do
    response = RestClient.get(url)
    more = JSON.parse(response)['has_more'].to_s
    if more == "true"
      pages.push url
      url = JSON.parse(response)['next_page']
    else
      pages.push url
      break 
    end
  end

  old_commanders = []

  pages.each do |x|
    response = RestClient.get(x)
    result = JSON.parse(response)['data']
    result.each do |i|
      old_commanders.push i['name']
    end
  end

  $players.each {|i| puts "#{i} builds [[#{old_commanders.sample}]]"}
end

def unCommander
  url = 'https://api.scryfall.com/cards/search?q=is%3Acommander+r%3Du'

  pages = []

  loop do
    response = RestClient.get(url)
    more = JSON.parse(response)['has_more'].to_s
    if more == "true"
      pages.push url
      url = JSON.parse(response)['next_page']
    else
      pages.push url
      break 
    end
  end

  un_commanders = []

  pages.each do |x|
    response = RestClient.get(x)
    result = JSON.parse(response)['data']
    result.each do |i|
      un_commanders.push i['name']
    end
  end

  $players.each {|i| puts "#{i} builds [[#{un_commanders.sample}]]"}
end

def rmPicker
  puts "Please enter a set code to search from. Set codes can be found at https://mtg.fandom.com/wiki/Set"

  set = gets.chomp!

  url = "https://api.scryfall.com/cards/search?q=set%3A#{set}+r%3E%3Dr"

  response = RestClient.get(url)

  result = JSON.parse(response)['data']

  rare_mythic = []

  result.each do |i| 
    rare_mythic.push i['name']
  end 

  $players.each {|i| puts "#{i} gets a copy of [[#{rare_mythic.sample}]]"}
end

def placePicker
  allSets = ["ARN", "ATQ", "LEG", "DRK", "FEM", "ICE", "HML", "ALL", "MIR", "VIS", "WTH", "TMP", "STH", "EXO", "USG", "ULG", "UDS", "MMQ", "NEM", "PCY", "INV", "PLS", "APC", "ODY", "TOR", "JUD", "ONS", "LGN", "SCG", "MRD", "DST", "5DN", "CHK", "BOK", "SOK", "RAV", "GPT", "DIS", "CSP", "TSP", "PLC", "FUT", "LRW", "MOR", "SHM", "EVE", "ALA", "CON", "ARB", "ZEN", "WWK", "ROE","SOM", "MBS", "NPH", "ISD", "DKA", "AVR", "RTR", "GTC", "DGM", "THS", "BNG", "JOU", "KTK", "FRF", "DTK", "BFZ", "OGW", "SOI", "EMN", "KLD", "AER", "AKH", "HOU", "XLN", "RIX", "DOM", "GRN", "RNA", "WAR", "ELD", "THB", "IKO", "ZNR", "KHM", "STX", "AFR", "MID", "VOW", "NEO", "SNC", "DMU", "BRO", "ONE", "MOM", "MAT", "LTR", "CMM", "WOE", "WHO", "LCI", "MKM", "PIP", "M11", "M12", "M13", "M14", "M15", "M16", "M17", "M18", "M19", "M20", "M21"]

  allBlocks = ["Ice Age", "Mirage", "Tempest", "Urza's", "Masques", "Invasion", "Odyssey", "Onslaught", "Mirrodin", "Kamigawa", "Ravnica", "Time Spiral", "Lorwyn", "Shadowmoor", "Alara", "Zendikar", "Scars of Mirrodin", "Innistrad", "Return to Ravnica", "Theros", "Khans of Tarkir", "Battle for Zendikar", "Shadows over Innistrad", "Kaladesh", "Amonkhet", "Ixalan"]

  allPlanes = ["Alara", "Amonkhet", "Arcavios", "Dominaria", "Eldraine", "Fiora", "Ikoria", "Innistrad", "Ixalan", "Kaladesh", "Kaldheim", "Lorwyn-Shadowmoor", "Mercadia", "Mirrodin", "New Capenna", "Ravnica", "Tarkir", "Theros", "Zendikar"] 

  puts "Please made a selection:"
  puts
  puts "(1) Random Set"
  puts "(2) Random Block"
  puts "(3) Random Plane"

  choice = gets.chomp!

  if choice == "1"
    puts "Your random set is #{allSets.sample}"
  elsif choice == "2"
    puts "Your random block is #{allBlocks.sample}"
  elsif choice == "3"
    puts "Your random plane is #{allPlanes.sample}"
  end   
end

puts "Please make a selection:"
puts
puts "(1) Random Commander with Background"
puts "(2) Random Commander under $0.79"
puts "(3) Random PreDH Commander"
puts "(4) Random Uncommon Commander"
puts "(5) Random Rare/Mythic Picker (For Sealed)"
puts "(6) Random Set, Block or Plane Picker"
puts "(7) Random Colour Picker"

choice = gets.chomp!

if choice == "1"
  background
elsif choice == "2"
  jankCommander
elsif choice == "3"
  oldCommander
elsif choice == "4"
  unCommander
elsif choice == "5"
  rmPicker
elsif choice == "6"
  placePicker
else
  colourPicker
end

