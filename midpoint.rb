#!/usr/bin/ruby

require 'rest-client'
require 'json'

url = 'https://api.scryfall.com/cards/search?q=is%3Acommander+f%3Ac+-is%3Areprint&order=released&dir=asc'

pages = []

loop do
  response = RestClient.get(url)
  more = JSON.parse(response)['has_more'].to_s
  if more == "true"
    pages.push url
    url = JSON.parse(response)['next_page']
  else
    pages.push url
    break 
  end
end

all_commander_names = []

pages.each do |x|
  response = RestClient.get(x)
  result = JSON.parse(response)['data']
  result.each do |i|
    all_commander_names.push ["#{i['name']}"]
  end
end

total = all_commander_names.count
median = total/2

puts all_commander_names[median-1]